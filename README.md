# slicefields

The slicefields crate provides a zero-cost way of using bit slices into fields of a struct as if they were real fields.  
It is extremely flexible, works with any types (not just Rust's built-ins) implementing the necessary traits for bitwise operations and has support for volatile reads/writes as well as access to unaligned pointers on all architectures.

## Performance

The goal of slicefields is to generate code which compiles down to as performant machine code
as possible, even without compiler optimizations enabled. While this certainly results in the
generated Rust code looking very weird, it should achieve its goal most of the time.  
The fantastic [Godbolt](https://godbolt.org) compiler explorer was used heavily during
the process of writing slicefields.

## Documentation

https://docs.rs/slicefields  
Examples for all of the above mentioned features can also be found in its [tests](tests/tests.rs)

## Contributing

This project is open to any sorts of contributions, be it by notifying me of an issue with the library
or by directly contributing code. Since I want to keep this library small and simple, however, you should
open an issue to discuss the changes you want to make before starting work on a pull request.

## License

Copyright 2023, TeleportAura  
This software is licensed under the [MPL 2.0](LICENSE) license
